<?php


class Ticket
{

    public string $NumderOfPlace;
    public string $NameOfFilm;
    public string $LastName;

    /**
     * Ticket constructor.
     * @param string $NumderOfPlace
     * @param string $NameOfFilm
     * @param string $LastName
     */
    public function __construct($NumderOfPlace, $NameOfFilm, $LastName)
    {
        $this->NumderOfPlace = $NumderOfPlace;
        $this->NameOfFilm = $NameOfFilm;
        $this->LastName = $LastName;
    }

    /**
     * @return string
     */
    public function getNumderOfPlace()
    {
        return $this->NumderOfPlace;
    }

    /**
     * @param string $NumderOfPlace
     */
    public function setNumderOfPlace($NumderOfPlace)
    {
        $this->NumderOfPlace = $NumderOfPlace;
    }

    /**
     * @return string
     */
    public function getNameOfFilm()
    {
        return $this->NameOfFilm;
    }

    /**
     * @param string $NameOfFilm
     */
    public function setNameOfFilm($NameOfFilm)
    {
        $this->NameOfFilm = $NameOfFilm;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->LastName;
    }

    /**
     * @param string $LastName
     */
    public function setLastName($LastName)
    {
        $this->LastName = $LastName;
    }


}