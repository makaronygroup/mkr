<?php
include 'src/Kinoteatr.php';

use \PHPUnit\Framework\TestCase;


class KinoteatrTest extends TestCase


{
    /**
     * @dataProvider addDataProvider
     * @param string $NumderOfPlace
     * @param string $NameOfFilm
     * @param string $LastName
     */

    public function testNumderOfPlaceByLastNames($LastName,$NumderOfPlace){
        $result = $this->Kinoteatr->add($LastName,$NumderOfPlace);
        $this->assertEquals($result);
    }

}